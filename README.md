# Maximum Likelihood Threshold Hunting according to Green 1993

This library implements the threshold hunting procedure described by [Green 1993](https://doi.org/10.1121/1.406696).

It basically stores a set of psychometric functions with different parameters:

1. "Mean", i.e. where the function's infliction point is. In a threshold paradigm with 0% false alarm rate, this is the 50% point.
2. False alarm rate.
3. Slope of the psychometric function.

The library provides a convenient class "MLThresholdHunter" who does all the background work.

It is the job of the developer to:

1. Choose meaningful parameters.
2. Stimulate the participant (or whatever you are stimulating!) at the level provided by the `current_probe_value` property.
3. Provide the answer (`true` or `false`) to the instance of the instance of the class.
4. Check whether `stop` is `true`.
5. If not, repeat from 2.
6. If `stop` is `true`, `current_guess` is your threshold.

# Reference

<a name="MLThresholdHunter"></a>

## MLThresholdHunter
**Kind**: global class  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| best_function | <code>PsychometricFunction</code> | The most likely psychometric function |
| current_probe_value | <code>number</code> | The value that should be probed next |
| current_guess | <code>number</code> | The current guess of the value at the infliction point |
| n_trials | <code>number</code> | The completed number of trials |
| guesses | <code>Array</code> | The guesses so far |
| std_last_trials | <code>number</code> | The stddev of the last use_n_trials_for_stop trials |
| stop_std | <code>number</code> | The stddev at which to stop |
| converged | <code>bool</code> | True if converged |
| stop | <code>bool</code> | True if either converged or maximum trials exceeded. |


* [MLThresholdHunter](#MLThresholdHunter)
    * [new MLThresholdHunter(start_value, values_range, false_alarm_range, slopes_range, min_trials, max_trials, use_n_trials_for_stop)](#new_MLThresholdHunter_new)
    * [.process_response(response, [value])](#MLThresholdHunter+process_response)

<a name="new_MLThresholdHunter_new"></a>

### new MLThresholdHunter(start_value, values_range, false_alarm_range, slopes_range, min_trials, max_trials, use_n_trials_for_stop)
Maximum Likelihood Threshold Hunting


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| start_value | <code>number</code> |  | The start value |
| values_range | <code>mathjs.matrix</code> \| <code>Array</code> |  | The possible values |
| false_alarm_range | <code>mathjs.matrix</code> \| <code>Array</code> |  | The possible false alarm rates |
| slopes_range | <code>mathjs.matrix</code> \| <code>Array</code> |  | The possible slopes |
| min_trials | <code>number</code> | <code>12</code> | The minimum amount of trials before the algorithm can stop. |
| max_trials | <code>number</code> | <code>40</code> | The maximum number of trials. |
| use_n_trials_for_stop | <code>number</code> | <code>6</code> | Use last n trials for stop criterion. |

<a name="MLThresholdHunter+process_response"></a>

### mlThresholdHunter.process\_response(response, [value])
Process a response

**Kind**: instance method of [<code>MLThresholdHunter</code>](#MLThresholdHunter)  

| Param | Type | Description |
| --- | --- | --- |
| response | <code>bool</code> | The response, true or false |
| [value] | <code>number</code> | The value at which the response was obtained.                              If undefined, use current_probe_value. |

