class PsychometricFunction {
    constructor(mean, false_alarm = 0, slope = 0.5) {
        this._mean = mean;
        this._false_alarm = false_alarm;
        this._slope = slope;
        this._p = 1;
    }

    get mean() {
        return this._mean;
    }

    get false_alarm() {
        return this._false_alarm;
    }

    get slope() {
        return this._slope;
    }

    get p() {
        return this._p;
    }

    get_value_from_p(p) {
        return (Math.log((this.false_alarm - p) / (p - 1)) + this.slope * this.mean) / this.slope;
    }

    get_p_from_value(value) {
        return this.false_alarm + (1 - this.false_alarm)
            * (1 / (1 + Math.exp(-1 * this.slope * (value - this.mean))));
    }

    get sweetpoint() {
        return (2 * this.false_alarm + 1 + Math.sqrt(1 + 8 * this.false_alarm))
            / (3 + Math.sqrt(1 + 8 * this.false_alarm));
    }

    get sweetpoint_value() {
        return this.get_value_from_p(this.sweetpoint);
    }

    process_response(response, value) {
        let x = this.get_p_from_value(value);
        if (!response) {
            x = 1 - x;
        }
        this._p *= x;
    }
}

export default PsychometricFunction;
