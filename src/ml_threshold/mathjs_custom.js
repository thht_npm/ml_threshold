import {
    matrix, min, std, max,
} from 'mathjs';

export default {
    matrix,
    max,
    min,
    std,
};
