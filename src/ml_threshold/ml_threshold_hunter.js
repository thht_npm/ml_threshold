import PsychometricFunction from './psychometric_function';
import math from './mathjs_custom';

class MLThresholdHunter {
    /**
     * Maximum Likelihood Threshold Hunting
     *
     * @param start_value {number} - The start value
     * @param values_range {(mathjs.matrix|Array)} - The possible values
     * @param false_alarm_range {(mathjs.matrix|Array)} - The possible false alarm rates
     * @param slopes_range {(mathjs.matrix|Array)} - The possible slopes
     * @param min_trials {number} - The minimum amount of trials before the algorithm can stop.
     * @param max_trials {number} - The maximum number of trials.
     * @param use_n_trials_for_stop {number} - Use last n trials for stop criterion.
     * @property best_function {PsychometricFunction} - The most likely psychometric function
     * @property current_probe_value {number} - The value that should be probed next
     * @property current_guess {number} - The current guess of the value at the infliction point
     * @property n_trials {number} - The completed number of trials
     * @property guesses {Array} - The guesses so far
     * @property std_last_trials {number} - The stddev of the last use_n_trials_for_stop trials
     * @property stop_std {number} - The stddev at which to stop
     * @property converged {bool} - True if converged
     * @property stop {bool} - True if either converged or maximum trials exceeded.
     */
    constructor(start_value, values_range, false_alarm_range,
        slopes_range, min_trials = 12, max_trials = 40, use_n_trials_for_stop = 6) {
        this._start_value = start_value;
        this._values_range = values_range;
        this._false_alarm_range = false_alarm_range;
        this._slopes_range = slopes_range;
        this._min_trials = min_trials;
        this._max_trials = max_trials;
        this._n_trials = 0;
        this._guesses = [];
        this._use_n_trials_for_stop = use_n_trials_for_stop;

        if (this._values_range.constructor.name !== 'Matrix') {
            this._values_range = math.matrix(this._values_range);
        }

        if (this._false_alarm_range.constructor.name !== 'Matrix') {
            this._false_alarm_range = math.matrix(this._false_alarm_range);
        }

        if (this._slopes_range.constructor.name !== 'Matrix') {
            this._slopes_range = math.matrix(this._slopes_range);
        }

        this._psyfuncs = [];

        const self = this;
        this._values_range.forEach((value) => {
            self._false_alarm_range.forEach((false_alarm) => {
                self._slopes_range.forEach((slope) => {
                    const pfun = new PsychometricFunction(value, false_alarm, slope);
                    self._psyfuncs.push(pfun);

                    if (false_alarm === math.min(self._false_alarm_range)
                       && slope === math.min(self._slopes_range)
                       && value === start_value) {
                        pfun._p = 1.000001;
                    }
                });
            });
        });
    }

    get best_function() {
        let best_p = 0;
        let cur_best = null;

        this._psyfuncs.forEach((cur_func) => {
            if (cur_func.p > best_p) {
                best_p = cur_func.p;
                cur_best = cur_func;
            }
        });

        return cur_best;
    }

    get current_probe_value() {
        return this.best_function.sweetpoint_value;
    }

    get current_guess() {
        return this.best_function.mean;
    }

    get n_trials() {
        return this._n_trials;
    }

    get guesses() {
        return this._guesses;
    }

    get std_last_trials() {
        if (this.guesses.length < this._use_n_trials_for_stop) {
            return NaN;
        }
        return math.std(this.guesses.slice(-this._use_n_trials_for_stop));
    }

    get stop_std() {
        return math.max(MLThresholdHunter._diff(this._values_range));
    }

    get converged() {
        return this.std_last_trials < this.stop_std;
    }

    get stop() {
        return (this._n_trials >= this._max_trials) || this.converged;
    }

    /**
     * Process a response
     * @param response {bool} - The response, true or false
     * @param value {number=} - The value at which the response was obtained.
     *                              If undefined, use current_probe_value.
     */
    process_response(response, value = undefined) {
        let cur_value = value;
        if (cur_value === undefined) {
            cur_value = this.current_probe_value;
        }
        this._n_trials += 1;

        this._psyfuncs.forEach((pfun) => {
            pfun.process_response(response, cur_value);
        });

        this._guesses.push(this.current_guess);
    }

    toString() {
        const retstr = (`MLThresholdHunter. Trial ${this.n_trials}/${this._max_trials}\n`
                 + `Current guess: ${this.current_guess} Current std: ${this.std_last_trials}`);

        return retstr;
    }

    static _diff(vals) {
        const diff = [];
        let old = null;

        vals.forEach((val) => {
            if (old !== null) {
                diff.push(val - old);
            }
            old = val;
        });

        return diff;
    }

    toJson() {
        return {
            n_trial: this.n_trials,
            max_trials: this._max_trials,
            current_guess: this.current_guess,
            current_probe_value: this.current_probe_value,
            current_std: this.std_last_trials,
        };
    }
}

export default MLThresholdHunter;
