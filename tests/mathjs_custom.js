import {
    range,
    matrix,
} from 'mathjs';

export default {
    range,
    matrix,
};
