import { MLThresholdHunter } from '../src';
import math from './mathjs_custom';

test('Can I create a usable MLThresholdHunter instance?', () => {
    const ml = new MLThresholdHunter(-20, [-40, -20, 0], [0],
        [0.5]);
    expect(ml).toBeInstanceOf(MLThresholdHunter);
});

test('Do a sample run and query most parameters', () => {
    const val_range = math.range(-100, 0, 0.5);
    const fa_range = math.range(0.1, 0.2, 0.02);
    const slope_range = math.matrix([0.5]);

    const ml = new MLThresholdHunter(-20, val_range, fa_range,
        slope_range);
    expect(ml._psyfuncs.length).toBe(
        val_range.size()[0] * fa_range.size()[0] * slope_range.size()[0],
    );

    let current_response = true;

    while (!ml.stop) {
        ml.process_response(current_response);
        current_response = !current_response;
    }

    expect(ml.n_trials).toBe(13);
    expect(ml.current_guess).toBe(-71.5);
    expect(ml.current_probe_value).toBeCloseTo(-71.18459061219569);
});
